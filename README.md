# K-means Sublevel Set problem and Optimality Interval

*  [How to tell when a clustering is (approximately) correct using convex relaxations](https://www.stat.washington.edu/mmp/Papers/sdp-kmeans-nips18.pdf)
*  [Slides](http://helper.ipam.ucla.edu/publications/mlpws3/mlpws3_15387.pdf)
*  [Block post on k-means SDP](http://solevillar.github.io/2016/07/05/Clustering-MNIST-SDP.html)