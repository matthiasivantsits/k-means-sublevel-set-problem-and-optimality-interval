import numpy as np


def svec(X):

    # shape of symmetric matrix and vector
    n = X.shape[0]
    n_ = int(n*(n+1)/2)

    #
    index_fx = lambda n: np.vstack((np.arange(n), np.ones((n)) * (n-1)))
    ixs = np.hstack([index_fx(i) for i in range(n+1)]).astype(np.int)

    #
    m_vec = np.empty((n_, ))
    sqrt2 = np.sqrt(2)
    for i, ix in enumerate(ixs.T):
        if ix[0] == ix[1]:
            m_vec[i] = X[ix[0], ix[1]]
        else:
            m_vec[i] = X[ix[0], ix[1]] * sqrt2

    return m_vec


def main():

    #
    n, dim = 50, 2
    k = 2
    n_ = int(n*(n+1)/2)

    #
    auxt = np.empty((n_, n+1))
    auxt[:,0] = svec(np.eye(n))
    b = np.ones((n+1, )) * 2
    b[0] = k

    for i in range(n):

        #
        A = np.zeros((n, n))
        A[:,i] = np.ones((1, n))
        A[i,:] = A[i,:] + np.ones((1, n))
        auxt[:,i+1] = svec(A)


if __name__ == '__main__':
    main()
